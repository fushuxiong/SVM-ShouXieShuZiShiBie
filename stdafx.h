// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once

#include <stdio.h>
#include <tchar.h>
#include<opencv/cv.h>
#include<opencv/highgui.h>
#include<vector>
#include <windows.h>
#include <stdlib.h>
#include <iostream>
using namespace std;
using namespace cv;  


// TODO: 在此处引用程序需要的其他头文件
class NumTrainData
{
public:
	NumTrainData()
	{
		memset(data, 0, sizeof(data));
		result = -1;
	}
public:
	float data[64];
	int result;
};